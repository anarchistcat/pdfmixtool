<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>PDF Mix Tool Hakkında</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Sürüm %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>PDF dosyalarını bölme, birleştirme, döndürme ve karıştırma yapan bir uygulama.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="70"/>
        <source>Website</source>
        <translation>Web sitesi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="77"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="94"/>
        <source>Authors</source>
        <translation>Yazarlar</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Çevirmenler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Emeği geçenler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Lisans</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Çekme isteği gönderin</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Hata bildir</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Çeviri yardımı</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Katkıda bulun</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Değişiklikler</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Çok sayfalı profili düzenle</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Sol</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Merkez</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Sağ</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Üst</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Adı:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Çıktı sayfa boyutu</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Özel boyut:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Sayfa düzeni</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Satırlar:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Sütunlar:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Aralık:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Sayfa hizalama</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Yatay:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Dikey:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Kenar boşlukları</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>PDF dosyaların özelliklerini düzenle</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Döndürme</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>TAMAM</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Çok sayfalı:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="57"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="200"/>
        <source>portrait</source>
        <translation>dikey</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="57"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="200"/>
        <source>landscape</source>
        <translation>yatay</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="157"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="218"/>
        <source>All</source>
        <translation>Tüm</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="237"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n sayfa</numerusform>
            <numerusform>%n sayfa</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="159"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>Pages:</source>
        <translation>Sayfalar:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="160"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>Multipage:</source>
        <translation>Çoklu sayfa:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="163"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="224"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="164"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="225"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="165"/>
        <source>Outline entry:</source>
        <translation>Anahat girişi:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="210"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="218"/>
        <source>New custom profile…</source>
        <translation>Yeni özel profil…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="220"/>
        <source>No rotation</source>
        <translation>Döndürme</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="226"/>
        <source>Pages:</source>
        <translation>Sayfalar:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="228"/>
        <source>Multipage:</source>
        <translation>Çok sayfalı:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="230"/>
        <source>Rotation:</source>
        <translation>Döndürme:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="232"/>
        <source>Outline entry:</source>
        <translation>Anahat girişi:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="189"/>
        <source>Add PDF file</source>
        <translation>PDF dosyası ekle</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>Move up</source>
        <translation>Yukarı Taşı</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <source>Move down</source>
        <translation>Aşağı taşı</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>Remove file</source>
        <translation>Dosyayı Kaldır</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="261"/>
        <location filename="../src/mainwindow.cpp" line="264"/>
        <source>Generate PDF</source>
        <translation>PDF oluştur</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="538"/>
        <location filename="../src/mainwindow.cpp" line="563"/>
        <source>PDF generation error</source>
        <translation>PDF oluşturma hatası</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="320"/>
        <source>Select one or more PDF files to open</source>
        <translation>Açmak için bir veya daha fazla PDF dosya seçin</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="179"/>
        <source>View</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <source>Main toolbar</source>
        <translation>Ana Araç Çubuğu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="237"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="242"/>
        <source>Multipage profiles…</source>
        <translation>Çok sayfalı profiller…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="252"/>
        <source>Exit</source>
        <translation>Çıkış</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="322"/>
        <location filename="../src/mainwindow.cpp" line="574"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF dosyalar (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="529"/>
        <source>Output pages: %1</source>
        <translation>Çıktı sayfaları: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="539"/>
        <source>You must add at least one PDF file.</source>
        <translation>En az bir PDF dosyası eklemelisiniz.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Dosya çıktı sayfaları %&lt;b&gt;%1&lt;/b&gt; kötü biçimlendirilmiş. Lütfen aşağıdaki kurallara uyduğunuzdan emin olun:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;sayfa aralıkları, ilk sayfayı ve son sayfayı bir çizgiyle ayrılmış olarak belirterek yazılmalıdır (örn. &quot;1-5&quot;&lt;/li&gt;); &lt;li&gt;tek sayfalar ve sayfaların aralıkları boşluklar, virgüller veya her ikisi ile ayrılmalıdır (örn. &quot;1, 2, 3, 5-10&quot; veya &quot;1 2 3 5-10&quot;&lt;/li&gt;); &lt;li&gt;tüm sayfalar ve sayfa aralıkları 1 ile PDF dosyasının sayfa sayısı arasında olmalıdır;&lt;/li&gt; &lt;li&gt; sadece sayılar, boşluklar, virgüller ve tireler kullanılabilir. Diğer tüm karakterlere izin verilmiyor.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="570"/>
        <source>Save PDF file</source>
        <translation>PDF dosyayı kaydet</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="34"/>
        <source>New profile…</source>
        <translation>Yeni profil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="38"/>
        <source>Delete profile</source>
        <translation>Profili sil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="46"/>
        <source>Manage multipage profiles</source>
        <translation>Çok sayfalı profilleri yönet</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="111"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="113"/>
        <source>Custom profile</source>
        <translation>Özel profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="165"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="174"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="188"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name can not be empty.</source>
        <translation>Profil adı boş olamaz.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="171"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="175"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <source>Profile name already exists.</source>
        <translation>Profil adı zaten var.</translation>
    </message>
</context>
</TS>
