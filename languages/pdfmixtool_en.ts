<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="70"/>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="77"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="94"/>
        <source>Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="57"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="200"/>
        <source>portrait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="57"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="200"/>
        <source>landscape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="157"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="218"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="237"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n page</numerusform>
            <numerusform>%n pages</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="159"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>Pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="160"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>Multipage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="163"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="224"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="164"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="225"/>
        <source>Rotation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="165"/>
        <source>Outline entry:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="210"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="218"/>
        <source>New custom profile…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="220"/>
        <source>No rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="226"/>
        <source>Pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="228"/>
        <source>Multipage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="230"/>
        <source>Rotation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="232"/>
        <source>Outline entry:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="189"/>
        <source>Add PDF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>Move up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <source>Move down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>Remove file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="261"/>
        <location filename="../src/mainwindow.cpp" line="264"/>
        <source>Generate PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="538"/>
        <location filename="../src/mainwindow.cpp" line="563"/>
        <source>PDF generation error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="320"/>
        <source>Select one or more PDF files to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="179"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <source>Main toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="237"/>
        <source>Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="242"/>
        <source>Multipage profiles…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="252"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="322"/>
        <location filename="../src/mainwindow.cpp" line="574"/>
        <source>PDF files (*.pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="529"/>
        <source>Output pages: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="539"/>
        <source>You must add at least one PDF file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="570"/>
        <source>Save PDF file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="34"/>
        <source>New profile…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="38"/>
        <source>Delete profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="46"/>
        <source>Manage multipage profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="111"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="113"/>
        <source>Custom profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="165"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="174"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="188"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name can not be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="171"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="175"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <source>Profile name already exists.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
