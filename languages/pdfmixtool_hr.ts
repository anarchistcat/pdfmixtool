<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hr">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>Informacije o PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Zatvori</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Verzija %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>Program za rastavljanje, spajanje, okretanje i miješanje PDF datoteka.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="70"/>
        <source>Website</source>
        <translation>WEB stranica</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="77"/>
        <source>About</source>
        <translation>Informacije</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="94"/>
        <source>Authors</source>
        <translation>Autori</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Prevoditelji</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Zasluge</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Licenca</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Pošalji zahtjev za povlačenje</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Prijavi grešku</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Pomozi prevoditi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Doprinesi</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Zapis promjena</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Uredi višestranični profil</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Lijevo</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Sredina</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Desno</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Gore</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Dolje</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Ime:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Izlazna veličina stranice</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Prilagođena veličina:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Raspored stranica</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Broj redaka:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Broj stupaca:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Razmak:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Poravnanje stranica</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Vodoravno:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Okomito:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Margine</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Uredi svojstva PDF datoteke</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Bez okretanja</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Odustani</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Višestranično:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="57"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="200"/>
        <source>portrait</source>
        <translation>uspravno</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="57"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="200"/>
        <source>landscape</source>
        <translation>polegnuto</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="157"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="218"/>
        <source>All</source>
        <translation>Sve</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="237"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n stranica</numerusform>
            <numerusform>%n stranice</numerusform>
            <numerusform>%n stranica</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="159"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>Pages:</source>
        <translation>Stranice:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="160"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>Multipage:</source>
        <translation>Višestranično:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="163"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="224"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="164"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="225"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="165"/>
        <source>Outline entry:</source>
        <translation>Unos strukture:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="210"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="218"/>
        <source>New custom profile…</source>
        <translation>Novi prilagođeni profil …</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="220"/>
        <source>No rotation</source>
        <translation>Bez okretanja</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="226"/>
        <source>Pages:</source>
        <translation>Stranice:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="228"/>
        <source>Multipage:</source>
        <translation>Višestranično:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="230"/>
        <source>Rotation:</source>
        <translation>Okretanje:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="232"/>
        <source>Outline entry:</source>
        <translation>Unos strukture:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="189"/>
        <source>Add PDF file</source>
        <translation>Dodaj PDF datoteku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>Move up</source>
        <translation>Premjesti gore</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <source>Move down</source>
        <translation>Premjesti dolje</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>Remove file</source>
        <translation>Ukloni datoteku</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>About</source>
        <translation>Informacije</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="261"/>
        <location filename="../src/mainwindow.cpp" line="264"/>
        <source>Generate PDF</source>
        <translation>Izradi PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="538"/>
        <location filename="../src/mainwindow.cpp" line="563"/>
        <source>PDF generation error</source>
        <translation>Greška tijekom izrade PDF-a</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="320"/>
        <source>Select one or more PDF files to open</source>
        <translation>Odaberi i otvori jednu ili više PDF datoteka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Edit</source>
        <translation>Uredi</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="179"/>
        <source>View</source>
        <translation>Pogledaj</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <source>Main toolbar</source>
        <translation>Glavna alatna traka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="237"/>
        <source>Menu</source>
        <translation>Izbornik</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="242"/>
        <source>Multipage profiles…</source>
        <translation>Višestranični profili …</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="252"/>
        <source>Exit</source>
        <translation>Izlaz</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="322"/>
        <location filename="../src/mainwindow.cpp" line="574"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF datoteke (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="529"/>
        <source>Output pages: %1</source>
        <translation>Broj izlaznih stranica: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="539"/>
        <source>You must add at least one PDF file.</source>
        <translation>Moraš dodati barem jednu PDF datoteku.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Izlazne stranice datoteke &lt;b&gt;%1&lt;/b&gt; su loše formatirane. Obavezno se drži sljedećih pravila:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervali stranica moraju se zadati s prvom i zadnjom stranicom, odvojene crticom (npr. „1-5”);&lt;/li&gt;&lt;li&gt;pojedinačne stranice i intervali stranica moraju se odvojiti s razmacima, sa zarezima ili s razmacima i zarezima (npr. „1, 2, 3, 5-10” ili „1 2 3 5-10”);&lt;/li&gt;&lt;li&gt;sve stranice i intervali stranica moraju biti između 1 i ukupnog broja stranica PDF datoteke;&lt;/li&gt;&lt;li&gt;mogu se koristiti samo brojevi, razmaci, zarezi i crtice. Svi drugi znakovi nisu dozvoljeni.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="570"/>
        <source>Save PDF file</source>
        <translation>Spremi PDF datoteku</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="34"/>
        <source>New profile…</source>
        <translation>Novi profil …</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="38"/>
        <source>Delete profile</source>
        <translation>Izbriši profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="46"/>
        <source>Manage multipage profiles</source>
        <translation>Upravljaj višestraničnim profilima</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="111"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="113"/>
        <source>Custom profile</source>
        <translation>Prilagođen profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="165"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="174"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="188"/>
        <source>Error</source>
        <translation>Greška</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name can not be empty.</source>
        <translation>Ime profila ne smije biti prazno.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="171"/>
        <source>Disabled</source>
        <translation>Deaktivirano</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="175"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <source>Profile name already exists.</source>
        <translation>Ime profila već postoji.</translation>
    </message>
</context>
</TS>
