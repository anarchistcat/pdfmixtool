<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>Über PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="44"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="55"/>
        <source>Version %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>An application to split, merge, rotate and mix PDF files.</source>
        <translation>Eine Anwendung zum Aufteilen, Zusammenfügen, Drehen und Mischen von PDF-Dateien.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="70"/>
        <source>Website</source>
        <translation>Webseite</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="77"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="94"/>
        <source>Authors</source>
        <translation>Autoren</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="96"/>
        <source>Translators</source>
        <translation>Übersetzer</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="105"/>
        <source>Credits</source>
        <translation>Beiträge</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="129"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="142"/>
        <source>Submit a pull request</source>
        <translation>Sende einen Pull-Request</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="143"/>
        <source>Report a bug</source>
        <translation>Melde einen Fehler</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Help translating</source>
        <translation>Beim Übersetzen helfen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="151"/>
        <source>Contribute</source>
        <translation>Beitragen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="168"/>
        <source>Changelog</source>
        <translation>Änderungsprotokoll</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Ändere Mehrseiten-Profil</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="64"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="166"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="65"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="69"/>
        <source>Center</source>
        <translation>Zentriert</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="66"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="168"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="68"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="170"/>
        <source>Top</source>
        <translation>Oben</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="70"/>
        <location filename="../src/editmultipageprofiledialog.cpp" line="172"/>
        <source>Bottom</source>
        <translation>Unten</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="101"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="109"/>
        <source>Output page size</source>
        <translation>Papierformat des Output</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="115"/>
        <source>Custom size:</source>
        <translation>Eigene Größe:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="129"/>
        <source>Pages layout</source>
        <translation>Seiten-Layout</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="132"/>
        <source>Rows:</source>
        <translation>Zeilen:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="135"/>
        <source>Columns:</source>
        <translation>Spalten:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="138"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="141"/>
        <source>Spacing:</source>
        <translation>Abstand:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="149"/>
        <source>Pages alignment</source>
        <translation>Seiten ausrichten</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="152"/>
        <source>Horizontal:</source>
        <translation>Horizontal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="155"/>
        <source>Vertical:</source>
        <translation>Vertikal:</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="163"/>
        <source>Margins</source>
        <translation>Ränder</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Bearbeite Eigenschaften der PDF-Dateien</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Keine Drehung</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Mehrseiten:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="57"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="200"/>
        <source>portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="57"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="200"/>
        <source>landscape</source>
        <translation>Quer</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="157"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="218"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/inputpdffiledelegate.cpp" line="126"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="237"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n Seite(n)</numerusform>
            <numerusform>%n Seiten</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="159"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="220"/>
        <source>Pages:</source>
        <translation>Seiten:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="160"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="221"/>
        <source>Multipage:</source>
        <translation>Mehrseitiges:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="163"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="224"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="164"/>
        <location filename="../src/inputpdffiledelegate.cpp" line="225"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="165"/>
        <source>Outline entry:</source>
        <translation>Gliederungseintrag:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="210"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="218"/>
        <source>New custom profile…</source>
        <translation>Neues eigenes Profil …</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="220"/>
        <source>No rotation</source>
        <translation>Keine Drehung</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="226"/>
        <source>Pages:</source>
        <translation>Seiten:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="228"/>
        <source>Multipage:</source>
        <translation>Mehrseitiges:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="230"/>
        <source>Rotation:</source>
        <translation>Drehung:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="232"/>
        <source>Outline entry:</source>
        <translation>Gliederungseintrag:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="189"/>
        <source>Add PDF file</source>
        <translation>PDF-Datei hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="194"/>
        <source>Move up</source>
        <translation>Nach oben verschieben</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <source>Move down</source>
        <translation>Nach unten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="204"/>
        <source>Remove file</source>
        <translation>Datei entfernen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="261"/>
        <location filename="../src/mainwindow.cpp" line="264"/>
        <source>Generate PDF</source>
        <translation>PDF-Datei erzeugen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="538"/>
        <location filename="../src/mainwindow.cpp" line="563"/>
        <source>PDF generation error</source>
        <translation>Fehler beim Erzeugen des PDF</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="320"/>
        <source>Select one or more PDF files to open</source>
        <translation>Wähle ein oder mehrere PDF-Dateien zum Öffnen</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="179"/>
        <source>View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="182"/>
        <source>Main toolbar</source>
        <translation>Hauptwerkzeugleiste</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="237"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="242"/>
        <source>Multipage profiles…</source>
        <translation>Mehrseiten-Profile…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="252"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="322"/>
        <location filename="../src/mainwindow.cpp" line="574"/>
        <source>PDF files (*.pdf)</source>
        <translation>PDF-Dateien (*.pdf)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="529"/>
        <source>Output pages: %1</source>
        <translation>Ausgabe-Seiten: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="539"/>
        <source>You must add at least one PDF file.</source>
        <translation>Sie müssen mindestens eine PDF-Datei hinzufügen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="546"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Ausgabeseiten der Datei &lt;b&gt;%1&lt;/b&gt; sind schlecht formatiert. Bitte stellen Sie sicher, dass Sie die folgenden Regeln eingehalten haben:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Intervalle von Seiten müssen geschrieben werden, wobei die erste Seite und die letzte Seite durch einen Bindestrich getrennt sind (z.B. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;Einzelseiten und Intervalle von Seiten müssen durch Leerzeichen, Kommas oder beides getrennt sein (z.B.g. &quot;1, 2, 3, 5-10&quot; oder &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;alle Seiten und Intervalle von Seiten müssen zwischen 1 und der Seitenzahl der PDF-Datei liegen;&lt;/li&gt;&lt;li&gt;nur Zahlen, Leerzeichen, Kommas und Bindestriche können verwendet werden. Alle anderen Zeichen sind nicht erlaubt.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="570"/>
        <source>Save PDF file</source>
        <translation>PDF-Datei abspeichern</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="34"/>
        <source>New profile…</source>
        <translation>Neues Profil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="38"/>
        <source>Delete profile</source>
        <translation>Profil löschen</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="46"/>
        <source>Manage multipage profiles</source>
        <translation>Verwalte Mehrseiten-Profile</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="111"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="113"/>
        <source>Custom profile</source>
        <translation>Eigenes Profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="165"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="174"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="188"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="166"/>
        <source>Profile name can not be empty.</source>
        <translation>Profilname darf nicht leer sein.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="171"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="175"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <source>Profile name already exists.</source>
        <translation>Der Profilname existiert bereits.</translation>
    </message>
</context>
</TS>
